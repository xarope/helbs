<?php

/**
* 
*/
class MCadastroLojista extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

public function insert_lojista($param){

		$campos = array(
			'id' => $param['num']+1,
			'CNPJ' => $param['cnpj'],
			'email' => $param['email'],
			'senha' => $param['pass'],
			'usuario' => $param['username']
			);


		$this->db->insert('login_lojista',$campos);
		if ($this->db->affected_rows() == 1)
		{
			return 1;
		}else{
			return 0;
		}
	}

public function contarlinhasLogin()
		{	
			$this->db->select ('COUNT("id") as num',false);
			$this->db->from('login_lojista');
			return $this->db->get()->result();
		}

}