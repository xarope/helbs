<?php

/**
* 
*/
class MLoginFornecedor extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function contarlinhasLogin()
		{	
			$this->db->select ('COUNT("id") as num',false);
			$this->db->from('login_fornecedor');
			return $this->db->get()->result();
		}

	public function conferirLogin()
	{		
		$this->db->select ('"usuario","senha","email"',false);
		$this->db->from('login_fornecedor');
		return $this->db->get()->result();
	}
}