<?php

/**
* 
*/
class MLogadoFornecedor extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	function produtos($name){

	  $this->db->select("nome,preco,fornecedor,frete,prazo,qtd");
	  $this->db->from('produtos');
	  $this->db->where('fornecedor',$name);
	  $query = $this->db->get();
	  return $query->result();
 	}
}