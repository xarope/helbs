<?php

/**
* 
*/
class MCadastroProduto extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function contarlinhasLogin()
		{	
			$this->db->select ('COUNT("id") as num',false);
			$this->db->from('produtos');
			return $this->db->get()->result();
		}

	public function inserirProduto($param)
	{		
		$campos = array(
			'id' => $param['num']+1,
			'nome' => $param['nome_produto'],
			'preco' => $param['preco'],
			'qtd' => $param['peso'],
			'fornecedor' => $param['name'],
			'frete' => 0,
			'total' => $param['preco'],
			'prazo' => 3,
			'classe' => 'D',
			'recomend' => 4
			);


		$this->db->insert('produtos',$campos);
		if ($this->db->affected_rows() == 1)
		{
			return 1;
		}else{
			return 0;
		}
	}
}