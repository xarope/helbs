<?php

/**
* 
*/
class MLogadoLojista extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	function show_students(){
		$this->db->distinct();
		$this->db->select('nome');
		$this->db->from('produtos');
		return $this->db->get()->result();
	}

	public function oferta($param){
		//preco
		//recomend
		//prazo
			if($param['preco'] == 'um' && $param['recomend'] == 'dois' && $param['prazo'] == 'tres')
	 		{
			$this->db->where("nome",$param['name1']);
			$this->db->select('id,nome,preco,frete,recomend,fornecedor,total,qtd,prazo');
			$this->db->from('produtos');
			$this->db->order_by('preco','asc','recomend','desc','prazo','asc');
			return $this->db->get()->result();
			}
			//recomend
			//preco
			//prazo
			if($param['preco'] == 'dois' && $param['recomend'] == 'um' && $param['prazo'] == 'tres')
	 		{
			$this->db->where("nome",$param['name1']);
			$this->db->select('id,nome,preco,frete,recomend,fornecedor,total,qtd,prazo');
			$this->db->from('produtos');
			$this->db->order_by('recomend','desc','preco','asc','prazo','asc');
			return $this->db->get()->result();
			}
			//recomend
			//prazo
			//preco
			if($param['preco'] == 'tres' && $param['recomend'] == 'um' && $param['prazo'] == 'dois')
	 		{
			$this->db->where("nome",$param['name1']);
			$this->db->select('id,nome,preco,frete,recomend,fornecedor,total,qtd,prazo');
			$this->db->from('produtos');
			$this->db->order_by('recomend','desc','prazo,preco','asc');
			return $this->db->get()->result();
			}
			//prazo
			//rec
			//preco
			if($param['preco'] == 'tres' && $param['recomend'] == 'dois' && $param['prazo'] == 'um')
	 		{
			$this->db->where("nome",$param['name1']);
			$this->db->select('id,nome,preco,frete,recomend,fornecedor,total,qtd,prazo');
			$this->db->from('produtos');
			$this->db->order_by('prazo','asc','recomend','desc','preco','asc');
			return $this->db->get()->result();
			}
			//preço
			//prazo
			//rec
			if($param['preco'] == 'um' && $param['recomend'] == 'tres' && $param['prazo'] == 'dois')
	 		{
			$this->db->where("nome",$param['name1']);
			$this->db->select('id,nome,preco,frete,recomend,fornecedor,total,qtd,prazo');
			$this->db->from('produtos');
			$this->db->order_by('preco,prazo','asc','recomend','desc');
			return $this->db->get()->result();
			}
			//prazo
			//preco
			//rec
			if($param['preco'] == 'dois' && $param['recomend'] == 'tres' && $param['prazo'] == 'um')
	 		{
			$this->db->where("nome",$param['name1']);
			$this->db->select('id,nome,preco,frete,recomend,fornecedor,total,qtd,prazo');
			$this->db->from('produtos');
			$this->db->order_by('prazo,preco','asc','recomend','desc');
			return $this->db->get()->result();
			}

	}

	public function oferta2($param){
		//preco
		//recomend
		//prazo
			if($param['preco'] == 'um' && $param['recomend'] == 'dois' && $param['prazo'] == 'tres')
	 		{
			$this->db->where("nome",$param['name2']);
			$this->db->select('id,nome,preco,frete,recomend,fornecedor,total,qtd,prazo');
			$this->db->from('produtos');
			$this->db->order_by('preco','asc','recomend','desc','prazo','asc');
			return $this->db->get()->result();
			}
			//recomend
			//preco
			//prazo
			if($param['preco'] == 'dois' && $param['recomend'] == 'um' && $param['prazo'] == 'tres')
	 		{
			$this->db->where("nome",$param['name2']);
			$this->db->select('id,nome,preco,frete,recomend,fornecedor,total,qtd,prazo');
			$this->db->from('produtos');
			$this->db->order_by('recomend','desc','preco','asc','prazo','asc');
			return $this->db->get()->result();
			}
			//recomend
			//prazo
			//preco
			if($param['preco'] == 'tres' && $param['recomend'] == 'um' && $param['prazo'] == 'dois')
	 		{
			$this->db->where("nome",$param['name2']);
			$this->db->select('id,nome,preco,frete,recomend,fornecedor,total,qtd,prazo');
			$this->db->from('produtos');
			$this->db->order_by('recomend','desc','prazo,preco','asc');
			return $this->db->get()->result();
			}
			//prazo
			//rec
			//preco
			if($param['preco'] == 'tres' && $param['recomend'] == 'dois' && $param['prazo'] == 'um')
	 		{
			$this->db->where("nome",$param['name2']);
			$this->db->select('id,nome,preco,frete,recomend,fornecedor,total,qtd,prazo');
			$this->db->from('produtos');
			$this->db->order_by('prazo','asc','recomend','desc','preco','asc');
			return $this->db->get()->result();
			}
			//preço
			//prazo
			//rec
			if($param['preco'] == 'um' && $param['recomend'] == 'tres' && $param['prazo'] == 'dois')
	 		{
			$this->db->where("nome",$param['name2']);
			$this->db->select('id,nome,preco,frete,recomend,fornecedor,total,qtd,prazo');
			$this->db->from('produtos');
			$this->db->order_by('preco,prazo','asc','recomend','desc');
			return $this->db->get()->result();
			}
			//prazo
			//preco
			//rec
			if($param['preco'] == 'dois' && $param['recomend'] == 'tres' && $param['prazo'] == 'um')
	 		{
			$this->db->where("nome",$param['name2']);
			$this->db->select('id,nome,preco,frete,recomend,fornecedor,total,qtd,prazo');
			$this->db->from('produtos');
			$this->db->order_by('prazo,preco','asc','recomend','desc');
			return $this->db->get()->result();
			}

	}


	public function oferta3($param){
		//preco
		//recomend
		//prazo
			if($param['preco'] == 'um' && $param['recomend'] == 'dois' && $param['prazo'] == 'tres')
	 		{
			$this->db->where("nome",$param['name3']);
			$this->db->select('id,nome,preco,frete,recomend,fornecedor,total,qtd,prazo');
			$this->db->from('produtos');
			$this->db->order_by('preco','asc','recomend','desc','prazo','asc');
			return $this->db->get()->result();
			}
			//recomend
			//preco
			//prazo
			if($param['preco'] == 'dois' && $param['recomend'] == 'um' && $param['prazo'] == 'tres')
	 		{
			$this->db->where("nome",$param['name3']);
			$this->db->select('id,nome,preco,frete,recomend,fornecedor,total,qtd,prazo');
			$this->db->from('produtos');
			$this->db->order_by('recomend','desc','preco','asc','prazo','asc');
			return $this->db->get()->result();
			}
			//recomend
			//prazo
			//preco
			if($param['preco'] == 'tres' && $param['recomend'] == 'um' && $param['prazo'] == 'dois')
	 		{
			$this->db->where("nome",$param['name3']);
			$this->db->select('id,nome,preco,frete,recomend,fornecedor,total,qtd,prazo');
			$this->db->from('produtos');
			$this->db->order_by('recomend','desc','prazo,preco','asc');
			return $this->db->get()->result();
			}
			//prazo
			//rec
			//preco
			if($param['preco'] == 'tres' && $param['recomend'] == 'dois' && $param['prazo'] == 'um')
	 		{
			$this->db->where("nome",$param['name3']);
			$this->db->select('id,nome,preco,frete,recomend,fornecedor,total,qtd,prazo');
			$this->db->from('produtos');
			$this->db->order_by('prazo','asc','recomend','desc','preco','asc');
			return $this->db->get()->result();
			}
			//preço
			//prazo
			//rec
			if($param['preco'] == 'um' && $param['recomend'] == 'tres' && $param['prazo'] == 'dois')
	 		{
			$this->db->where("nome",$param['name3']);
			$this->db->select('id,nome,preco,frete,recomend,fornecedor,total,qtd,prazo');
			$this->db->from('produtos');
			$this->db->order_by('preco,prazo','asc','recomend','desc');
			return $this->db->get()->result();
			}
			//prazo
			//preco
			//rec
			if($param['preco'] == 'dois' && $param['recomend'] == 'tres' && $param['prazo'] == 'um')
	 		{
			$this->db->where("nome",$param['name3']);
			$this->db->select('id,nome,preco,frete,recomend,fornecedor,total,qtd,prazo');
			$this->db->from('produtos');
			$this->db->order_by('prazo,preco','asc','recomend','desc');
			return $this->db->get()->result();
			}

	}
}