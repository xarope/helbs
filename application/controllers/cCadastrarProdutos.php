<?php

class CCadastrarProdutos extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('MCadastroProduto');
	}

	public function index(){
		$this->load->view('vCadastrarProdutos');
	}

	public function contarlinhasLogin(){

		$r = $this-> MCadastroProduto -> contarlinhasLogin();
		echo json_encode($r);
	}

	public function inserirProduto()
	{
		$param['num'] = $this->input-> post('num');
		$param['nome_produto'] = $this->input-> post('nome_produto');
		$param['preco'] = $this->input-> post('preco');
		$param['peso'] = $this->input-> post('peso'); 
		$param['name'] = $this->input-> post('name'); 


		$r = $this-> MCadastroProduto -> inserirProduto($param);

		echo json_encode($r); 
	}
}