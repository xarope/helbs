<?php

class CLoginLojista extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('mLoginLojista');
	}

	public function index(){
		$this->load->view('vLoginLojista');
	}

	public function contarlinhasLogin(){

		$r = $this-> mLoginLojista -> contarlinhasLogin();
		echo json_encode($r);
	}

	public function conferirLogin()
	{
			$r = $this-> mLoginLojista ->conferirLogin();
			echo json_encode($r);
	}
}