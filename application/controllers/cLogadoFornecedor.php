<?php

class CLogadoFornecedor extends CI_Controller
{

	public $name;

	function __construct()
	{
		parent::__construct();
		$this->load->model('mLogadoFornecedor');
	}


	public function index(){ 
		  //echo '<script type="text/javascript">alert("teste2: ' . $_GET['name'] . '")</script>';
		  $name = $_GET['name'];
		  $query = $this-> mLogadoFornecedor ->produtos($name);
		  $data['DADOS'] = null;
		  if($query){
		   $data['DADOS'] =  $query;
		  }
		  $this->load->view('vLogadoFornecedor',$data);
	}

}