<?php

class CLoginFornecedor extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('MLoginFornecedor');
	}

	public function index(){
		$this->load->view('vLoginFornecedor');
	}

	public function contarlinhasLogin(){

		$r = $this-> MLoginFornecedor -> contarlinhasLogin();
		echo json_encode($r);
	}

	public function conferirLogin()
	{
			$r = $this-> MLoginFornecedor ->conferirLogin();
			echo json_encode($r);
	}
}