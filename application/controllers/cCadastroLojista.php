<?php

class CCadastroLojista extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('MCadastroLojista');
	}

	public function index(){
		$this->load->view('vCadastroLojista');
	}

	public function insert_lojista(){
		$param['num'] = $this->input-> post('num');
		$param['cnpj'] = $this->input-> post('cnpj');
		$param['email'] = $this->input-> post('email');
		$param['pass'] = $this->input-> post('pass');
		$param['username'] = $this->input-> post('username');

		$r = $this-> MCadastroLojista -> insert_lojista($param);

		echo json_encode($r); 
	}

	public function contarlinhasLogin(){

		$r = $this-> MCadastroLojista -> contarlinhasLogin();
		echo json_encode($r);
	}
}