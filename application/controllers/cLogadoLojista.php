<?php

class CLogadoLojista extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('mLogadoLojista');
	}

	public function index(){
		//$this->load->view('vLogadoLojista');

		$data['students'] = $this->mLogadoLojista->show_students();
        $this->load->view('vLogadoLojista', $data);  
	}

	public function oferta(){
		$param['posicao'] = $this -> input -> post('posicao');

		$param['name1'] = $this -> input -> post('name1');
		$param['name2'] = $this -> input -> post('name2');
		$param['name3'] = $this -> input -> post('name3');

		$param['qtd1'] = $this -> input -> post('qtd1');
		$param['qtd2'] = $this -> input -> post('qtd2');
		$param['qtd3'] = $this -> input -> post('qtd3');

		$param['preco'] = $this -> input -> post('preco');
		$param['prazo'] = $this -> input -> post('prazo');
		$param['recomend'] = $this -> input -> post('recomend');

		$r = $this->mLogadoLojista ->oferta($param);
		echo json_encode($r);
}

	public function oferta2(){
		$param['posicao'] = $this -> input -> post('posicao');

		$param['name1'] = $this -> input -> post('name1');
		$param['name2'] = $this -> input -> post('name2');
		$param['name3'] = $this -> input -> post('name3');

		$param['qtd1'] = $this -> input -> post('qtd1');
		$param['qtd2'] = $this -> input -> post('qtd2');
		$param['qtd3'] = $this -> input -> post('qtd3');

		$param['preco'] = $this -> input -> post('preco');
		$param['prazo'] = $this -> input -> post('prazo');
		$param['recomend'] = $this -> input -> post('recomend');

		$r = $this->mLogadoLojista ->oferta2($param);
		echo json_encode($r);
}

	public function oferta3(){
		$param['posicao'] = $this -> input -> post('posicao');

		$param['name1'] = $this -> input -> post('name1');
		$param['name2'] = $this -> input -> post('name2');
		$param['name3'] = $this -> input -> post('name3');

		$param['qtd1'] = $this -> input -> post('qtd1');
		$param['qtd2'] = $this -> input -> post('qtd2');
		$param['qtd3'] = $this -> input -> post('qtd3');

		$param['preco'] = $this -> input -> post('preco');
		$param['prazo'] = $this -> input -> post('prazo');
		$param['recomend'] = $this -> input -> post('recomend');

		$r = $this->mLogadoLojista ->oferta3($param);
		echo json_encode($r);
}

}