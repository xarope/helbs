<?php

class CCadastroFornecedor extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('MCadastroFornecedor');
	}

	public function index(){
		$this->load->view('vCadastroFornecedor');
	}

	public function insert_lojista(){
		$param['num'] = $this->input-> post('num');
		$param['cnpj'] = $this->input-> post('cnpj');
		$param['email'] = $this->input-> post('email');
		$param['pass'] = $this->input-> post('pass');
		$param['username'] = $this->input-> post('username');
		$param['nome'] = $this->input-> post('nome');
		$param['cep'] = $this->input-> post('cep');
		$param['endereco'] = $this->input-> post('endereco');
		$param['cpf'] = $this->input-> post('cpf');

		$r = $this-> MCadastroFornecedor -> insert_lojista($param);

		echo json_encode($r); 
	}

	public function contarlinhasLogin(){

		$r = $this-> MCadastroFornecedor -> contarlinhasLogin();
		echo json_encode($r);
	}
}