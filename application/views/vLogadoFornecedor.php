<!DOCTYPE html>
<html lang="en">
<head>
	<title>Helbs</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/bootstrap-chosen.css">
	<script src="<?php echo base_url();?>assets/vendor/jquery/jquery-3.2.1.min.js"></script>
    <script src="<?php echo base_url();?>assets/css/chosen.jquery.js"></script>
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="<?php echo base_url();?>assets/images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/fonts/iconic/css/material-design-iconic-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/vendor/select2/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/util.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/main.css">
<!--===============================================================================================-->
<style>
#boasvindas{
    top: 10px;
    right: 0;
    padding: 10px;
    border: 3px solid #0300a0;
    color:#0300a0;
}
.esquerda{
	float:left;
}
.input101 {
  font-family: Poppins-Medium;
  font-size: 16px;
  color: #333333;
  line-height: 1.2;

  width:70%;
  height: 55px;
  background: transparent;
  padding: 0 7px 0 43px;
}
.teste{
    background-color: #fff;
    -webkit-background-clip: padding-box;
    -moz-background-clip: padding;
    background-clip: padding-box;
    border: 1px solid #ccc;
    border-top-right-radius: 4px;
    border-top-left-radius: 4px;
    border-bottom-right-radius: 4px;
    border-bottom-left-radius: 4px;
    -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
    box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
    color: #555555;
    display: block;
    width:100%;
    height: 34px;
    overflow: hidden;
    line-height: 34px;
    padding: 0 0 0 8px;
    position: relative;
    text-decoration: none;
    white-space: nowrap;
    font-family: Poppins-Regular;
    font-size: 14px;
    line-height: 1.7;
    margin: 0px;
    transition: all 0.4s;
    -webkit-transition: all 0.4s;
    -o-transition: all 0.4s;
    -moz-transition: all 0.4s;
}
.pad{
	padding-bottom: 30px;
}
.letra{
	font-family: Poppins-Regular;
    font-size: 14px;
    line-height: 1.7;
}
.padd{
	padding-top: 30px;
}
.tamanho{
	width:50%!important;
}
#new_{
	font-size: 12px;
}
</style>
<script>
$(function() {
	$('.chosen-select').chosen();
	$('.chosen-select-deselect').chosen({ allow_single_deselect: true });
});

</script>
</head>
<body>
<!-- Columns are always 50% wide, on mobile and desktop -->
	<div class="limiter">
		<div class="container-login100" style="background-image: url('<?php echo base_url();?>assets/images/bg-01.jpg');">
			<div class="wrap-login100 p-l-55 p-r-55 p-t-65 p-b-54">
				<div class="login100-form validate-form">
					<span class="login100-form-title p-b-49">
						<div id='boasvindas'> </div>
						<br>
						<p> Produtos </p>
							<div id="new_">
							 	 <div class="row">
								   <div style="width:800px;margin:20px;">
								    <table class="table table-striped table-bordered">
								     <tr>
								     	<td><strong>Produto</strong></td>
								     	<td><strong>Preco</strong></td>
								     	<td><strong>Fornecedor</strong></td>
								     	<td><strong>Prazo</strong></td>
								     	<td><strong>Peso</strong></td>
								     </tr> 
								     <?php foreach($DADOS as $dado){?>
								     <tr>
								     	<td><?=$dado->nome;?></td>
								     	<td>R$ <?=$dado->preco;?>,00</td>
								     	<td><?=$dado->fornecedor;?></td>
								     	<td>R$ <?=$dado->prazo;?>,00</td>
								     	<td><?=$dado->qtd;?> KG</td>
								     </tr>    
								        <?php }?>  
								    </table>
								   </div> 
								  </div> 
							</div>
						<button type="button" class="btn btn-success" id="mais">+ Cadastrar novo produto</button>
					</span>
				</div>
			</div>
		</div>
	</div>
	</div>
<!--===============================================================================================-->
<!--===============================================================================================-->
	<script src="<?php echo base_url();?>assets/vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url();?>assets/vendor/bootstrap/js/popper.js"></script>
	<script src="<?php echo base_url();?>assets/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url();?>assets/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url();?>assets/vendor/daterangepicker/moment.min.js"></script>
	<script src="<?php echo base_url();?>assets/vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url();?>assets/vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url();?>assets/js/main.js"></script>
<!--===============================================================================================-->
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<!--===============================================================================================-->

<script>
document.getElementById("boasvindas").innerHTML = "Olá, " + name;
$("#mais").click(function(){
	window.location.href = '<?php echo base_url();?>cCadastrarProdutos?name='+name;
});
</script>
</body>
</html>