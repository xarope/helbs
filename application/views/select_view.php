
<html>
    <head>
        <title> Create And Validate Select Option Field (using for each loop) In CodeIgniter</title>
        <link href='http://fonts.googleapis.com/css?family=Marcellus' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(). "css/select.css" ?>">
    </head>
    <body>
        <div id="container">
                <h3>Create And Validate Select Option Field (using for each loop) In CodeIgniter</h3><hr/> 

			    <select name="city">
                    <option value="none" selected="selected">------------Select City------------</option>
         
		<!---Displaying fetched cities in options using foreach loop -->
					<?php foreach($students as $student):?> 
                    <option value="<?php echo $student->student_id?>"><?php echo $student->student_city?></option>
                    <?php endforeach;?>  
                </select>
           


	    <!--Right side advertisement div-->
            <div id="fugo">
                <a href="http://www.formget.com/app/"><img src="<?php echo base_url(). "images/formGetadv-1.jpg" ?>" alt="FormGet"/></a>  
            </div>
        </div>
    </body>
</html>