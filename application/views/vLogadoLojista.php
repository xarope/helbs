<!DOCTYPE html>
<html lang="en">
<head>
	<title>Helbs</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/bootstrap-chosen.css">
	<script src="<?php echo base_url();?>assets/vendor/jquery/jquery-3.2.1.min.js"></script>
    <script src="<?php echo base_url();?>assets/css/chosen.jquery.js"></script>
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="<?php echo base_url();?>assets/images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/fonts/iconic/css/material-design-iconic-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/vendor/select2/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/util.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/main.css">
<!--===============================================================================================-->
<style>
#boasvindas{
    top: 10px;
    right: 0;
    padding: 10px;
    border: 3px solid #0300a0;
    color:#0300a0;
}
.esquerda{
	float:left;
}
.input101 {
  font-family: Poppins-Medium;
  font-size: 16px;
  color: #333333;
  line-height: 1.2;

  width:70%;
  height: 55px;
  background: transparent;
  padding: 0 7px 0 43px;
}
.teste{
    background-color: #fff;
    -webkit-background-clip: padding-box;
    -moz-background-clip: padding;
    background-clip: padding-box;
    border: 1px solid #ccc;
    border-top-right-radius: 4px;
    border-top-left-radius: 4px;
    border-bottom-right-radius: 4px;
    border-bottom-left-radius: 4px;
    -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
    box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
    color: #555555;
    display: block;
    width:100%;
    height: 34px;
    overflow: hidden;
    line-height: 34px;
    padding: 0 0 0 8px;
    position: relative;
    text-decoration: none;
    white-space: nowrap;
    font-family: Poppins-Regular;
    font-size: 14px;
    line-height: 1.7;
    margin: 0px;
    transition: all 0.4s;
    -webkit-transition: all 0.4s;
    -o-transition: all 0.4s;
    -moz-transition: all 0.4s;
}
.pad{
	padding-bottom: 30px;
}
.letra{
	font-family: Poppins-Regular;
    font-size: 14px;
    line-height: 1.7;
}
.padd{
	padding-top: 30px;
}
.tamanho{
	width:50%!important;
}

</style>
<script>
$(function() {
	$('.chosen-select').chosen();
	$('.chosen-select-deselect').chosen({ allow_single_deselect: true });
});

</script>
</head>
<body>
<!-- Columns are always 50% wide, on mobile and desktop -->
	<div class="limiter">
		<div class="container-login100" style="background-image: url('<?php echo base_url();?>assets/images/bg-01.jpg');">
			<div class="wrap-login100 p-l-55 p-r-55 p-t-65 p-b-54">
				<div class="login100-form validate-form">
					<span class="login100-form-title p-b-49">
						<div id='boasvindas'> </div>

							<p class='padd letra'>Grau de importância PREÇO:</p>
							<select data-placeholder="Grau de importância PREÇO" id="preco" class="chosen-select" >
							  <option value="none"> </option>
							  <option value="um">1</option>
							  <option value="dois">2</option>
							  <option value="tres">3</option>
							</select>
							<p class='letra'>Grau de importância RECOMENDAÇÕES:</p>
							<select data-placeholder="Grau de importância RECOMENDAÇÕES" id="recomend" class="chosen-select" >
							  <option value="none"> </option>
							  <option value="um">1</option>
							  <option value="dois">2</option>
							  <option value="tres">3</option>
							</select>
							<p class='letra'>Grau de importância PRAZO:</p>
							<select data-placeholder="Grau de importância PRAZO" id="prazo" class="chosen-select" >
							  <option value="none"> </option>
							  <option value="um">1</option>
							  <option value="dois">2</option>
							  <option value="tres">3</option>
							</select>

							<hr width="100%">

							<div id="tudo1">
							<select data-placeholder="Escolha o produto" class="chosen-select" tabindex="2" id='name1'>
			                    <option value="none" selected="selected">------------Selecione o Produto------------</option>
								<?php foreach($students as $student):?> 
			                    <option value="<?php echo $student->nome?>"><?php echo $student->nome?></option>
			                    <?php endforeach;?>  
				            </select>
				            <div class='pad'>
								<input type="number" class="teste" name="username" placeholder="Defina o peso em Kilos" id='qtd1'>
							</div>
							</div>

							<div id="tudo2">
							<select data-placeholder="Escolha o produto" class="chosen-select" tabindex="2" id='name2'>
			                    <option value="none" selected="selected">------------Selecione o Produto------------</option>
								<?php foreach($students as $student):?> 
			                    <option value="<?php echo $student->nome?>"><?php echo $student->nome?></option>
			                    <?php endforeach;?>  
				            </select>
				            <div class='pad'>
								<input type="number" class="teste" name="username" placeholder="Defina o peso em Kilos" id='qtd2'>
								<button type="button" class="btn btn-danger" id="remover2">Remover</button>
							</div>
							</div>

							<div id="tudo3">
							<select data-placeholder="Escolha o produto" class="chosen-select" tabindex="2" id='name3'>
			                    <option value="none" selected="selected">------------Selecione o Produto------------</option>
								<?php foreach($students as $student):?> 
			                    <option value="<?php echo $student->nome?>"><?php echo $student->nome?></option>
			                    <?php endforeach;?>  
				            </select>
				            <div class='pad'>
								<input type="number" class="teste" name="username" placeholder="Defina o peso em Kilos" id='qtd3'>
								<button type="button" class="btn btn-danger" id="remover3">Remover</button>
							</div>
							</div>

							
							<div id="adicionar"></div>
							
						<button type="button" class="btn btn-success" id="mais">+</button>
					  <button type="button" class="btn btn-primary" id='pesquisar'>Pesquisar</button>
					</span>
				</div>
			</div>
		</div>
	</div>
	</div>
<!--===============================================================================================-->
	<!-- Modal -->
<div id="Modal" class="modal fade" role="dialog">
  <div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h4 class="modal-title">Melhor Oferta encontrada</h4>
  </div>
  <div class="modal-body">
  		<div id="resultado">
		<tr>
			<td><label class='tamanho'>Produto: </label><input class='tamanho' type="text" id="pro" disabled="disabled"></td>
		</tr>
		<tr>
			<td><label class='tamanho'>Peso (Kg): </label><input class='tamanho' type="text" id="qua" disabled="disabled"></td>
		</tr>
		<tr>
			<td><label class='tamanho'>Valor por Kg: </label><input class='tamanho' type="text" id="val" disabled="disabled"></td>
		</tr>
		<tr>
			<td><label class='tamanho'>Frete Total: </label><input class='tamanho' type="text" id="fre" disabled="disabled"></td>
		</tr>
		<tr>			
			<td><label class='tamanho'>Estrelas: </label><input class='tamanho' type="text" id="est" disabled="disabled"></td>
		</tr>
		<tr>
			<td><label class='tamanho'>Prazo: </label><input class='tamanho' type="text" id="pra" disabled="disabled"></td>
		</tr>
		<tr>
			<td><label class='tamanho'>Fornecedor: </label><input class='tamanho' type="text" id="for" disabled="disabled"></td>
		</tr>
		<tr>
			<td><label class='tamanho'>Total: </label><input class='tamanho' type="text" id="tot" disabled="disabled"></td>
		</tr>
		<hr width="100%">
		</div>

  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button>
    <button type="button" class="btn btn-success">Comprar</button>
  </div>
</div>

  </div>
</div>
<!--===============================================================================================-->
<!--===============================================================================================-->
	<!-- Modal -->
<div id="Modal2" class="modal fade" role="dialog">
  <div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h4 class="modal-title">Melhor Oferta encontrada</h4>
  </div>
  <div class="modal-body">
  		<div id="resultado">
		<tr>
			<td><label class='tamanho'>Produto: </label><input class='tamanho' type="text" id="pro21" disabled="disabled"></td>
		</tr>
		<tr>
			<td><label class='tamanho'>Peso (Kg): </label><input class='tamanho' type="text" id="qua21" disabled="disabled"></td>
		</tr>
		<tr>
			<td><label class='tamanho'>Valor por Kg: </label><input class='tamanho' type="text" id="val21" disabled="disabled"></td>
		</tr>
		<tr>
			<td><label class='tamanho'>Frete Total: </label><input class='tamanho' type="text" id="fre21" disabled="disabled"></td>
		</tr>
		<tr>			
			<td><label class='tamanho'>Estrelas: </label><input class='tamanho' type="text" id="est21" disabled="disabled"></td>
		</tr>
		<tr>
			<td><label class='tamanho'>Prazo: </label><input class='tamanho' type="text" id="pra21" disabled="disabled"></td>
		</tr>
		<tr>
			<td><label class='tamanho'>Fornecedor: </label><input class='tamanho' type="text" id="for21" disabled="disabled"></td>
		</tr>
		<tr>
			<td><label class='tamanho'>Total: </label><input class='tamanho' type="text" id="tot21" disabled="disabled"></td>
		</tr>
		<hr width="100%">


		<br>


		<tr>
			<td><label class='tamanho'>Produto: </label><input class='tamanho' type="text" id="pro22" disabled="disabled"></td>
		</tr>
		<tr>
			<td><label class='tamanho'>Peso (Kg): </label><input class='tamanho' type="text" id="qua22" disabled="disabled"></td>
		</tr>
		<tr>
			<td><label class='tamanho'>Valor por Kg: </label><input class='tamanho' type="text" id="val22" disabled="disabled"></td>
		</tr>
		<tr>
			<td><label class='tamanho'>Frete Total: </label><input class='tamanho' type="text" id="fre22" disabled="disabled"></td>
		</tr>
		<tr>			
			<td><label class='tamanho'>Estrelas: </label><input class='tamanho' type="text" id="est22" disabled="disabled"></td>
		</tr>
		<tr>
			<td><label class='tamanho'>Prazo: </label><input class='tamanho' type="text" id="pra22" disabled="disabled"></td>
		</tr>
		<tr>
			<td><label class='tamanho'>Fornecedor: </label><input class='tamanho' type="text" id="for22" disabled="disabled"></td>
		</tr>
		<tr>
			<td><label class='tamanho'>Total: </label><input class='tamanho' type="text" id="tot22" disabled="disabled"></td>
		</tr>
		<hr width="100%">
		</div>
		<br>

		<tr>
			<td><label class='tamanho'>Total Geral: </label><input class='tamanho' type="text" id="totgeral" disabled="disabled"></td>
		</tr>

  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button>
    <button type="button" class="btn btn-success">Comprar</button>
  </div>
</div>

  </div>
</div>
<!--===============================================================================================-->
	<!-- Modal -->
<div id="Modal3" class="modal fade" role="dialog">
  <div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h4 class="modal-title">Melhor Oferta encontrada</h4>
  </div>
  <div class="modal-body">
  		<div id="resultado">
		<tr>
			<td><label class='tamanho'>Produto: </label><input class='tamanho' type="text" id="pro31" disabled="disabled"></td>
		</tr>
		<tr>
			<td><label class='tamanho'>Peso (Kg): </label><input class='tamanho' type="text" id="qua31" disabled="disabled"></td>
		</tr>
		<tr>
			<td><label class='tamanho'>Valor por Kg: </label><input class='tamanho' type="text" id="val31" disabled="disabled"></td>
		</tr>
		<tr>
			<td><label class='tamanho'>Frete Total: </label><input class='tamanho' type="text" id="fre31" disabled="disabled"></td>
		</tr>
		<tr>			
			<td><label class='tamanho'>Estrelas: </label><input class='tamanho' type="text" id="est31" disabled="disabled"></td>
		</tr>
		<tr>
			<td><label class='tamanho'>Prazo: </label><input class='tamanho' type="text" id="pra31" disabled="disabled"></td>
		</tr>
		<tr>
			<td><label class='tamanho'>Fornecedor: </label><input class='tamanho' type="text" id="for31" disabled="disabled"></td>
		</tr>
		<tr>
			<td><label class='tamanho'>Total: </label><input class='tamanho' type="text" id="tot31" disabled="disabled"></td>
		</tr>
		<hr width="100%">


		<br>


		<tr>
			<td><label class='tamanho'>Produto: </label><input class='tamanho' type="text" id="pro32" disabled="disabled"></td>
		</tr>
		<tr>
			<td><label class='tamanho'>Peso (Kg): </label><input class='tamanho' type="text" id="qua32" disabled="disabled"></td>
		</tr>
		<tr>
			<td><label class='tamanho'>Valor por Kg: </label><input class='tamanho' type="text" id="val32" disabled="disabled"></td>
		</tr>
		<tr>
			<td><label class='tamanho'>Frete Total: </label><input class='tamanho' type="text" id="fre32" disabled="disabled"></td>
		</tr>
		<tr>			
			<td><label class='tamanho'>Estrelas: </label><input class='tamanho' type="text" id="est32" disabled="disabled"></td>
		</tr>
		<tr>
			<td><label class='tamanho'>Prazo: </label><input class='tamanho' type="text" id="pra32" disabled="disabled"></td>
		</tr>
		<tr>
			<td><label class='tamanho'>Fornecedor: </label><input class='tamanho' type="text" id="for32" disabled="disabled"></td>
		</tr>
		<tr>
			<td><label class='tamanho'>Total: </label><input class='tamanho' type="text" id="tot32" disabled="disabled"></td>
		</tr>
		<hr width="100%">
		</div>
		<br>

		<br>


		<tr>
			<td><label class='tamanho'>Produto: </label><input class='tamanho' type="text" id="pro33" disabled="disabled"></td>
		</tr>
		<tr>
			<td><label class='tamanho'>Peso (Kg): </label><input class='tamanho' type="text" id="qua33" disabled="disabled"></td>
		</tr>
		<tr>
			<td><label class='tamanho'>Valor por Kg: </label><input class='tamanho' type="text" id="val33" disabled="disabled"></td>
		</tr>
		<tr>
			<td><label class='tamanho'>Frete Total: </label><input class='tamanho' type="text" id="fre33" disabled="disabled"></td>
		</tr>
		<tr>			
			<td><label class='tamanho'>Estrelas: </label><input class='tamanho' type="text" id="est33" disabled="disabled"></td>
		</tr>
		<tr>
			<td><label class='tamanho'>Prazo: </label><input class='tamanho' type="text" id="pra33" disabled="disabled"></td>
		</tr>
		<tr>
			<td><label class='tamanho'>Fornecedor: </label><input class='tamanho' type="text" id="for33" disabled="disabled"></td>
		</tr>
		<tr>
			<td><label class='tamanho'>Total: </label><input class='tamanho' type="text" id="tot33" disabled="disabled"></td>
		</tr>
		<hr width="100%">
		</div>
		
		<tr>
			<td><label class='tamanho'>Total Geral: </label><input class='tamanho' type="text" id="totgeral2" disabled="disabled"></td>
		</tr>

  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button>
    <button type="button" class="btn btn-success">Comprar</button>
  </div>
</div>

  </div>
</div>
<!--===============================================================================================-->
<!--===============================================================================================-->
	<script src="<?php echo base_url();?>assets/vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url();?>assets/vendor/bootstrap/js/popper.js"></script>
	<script src="<?php echo base_url();?>assets/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url();?>assets/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url();?>assets/vendor/daterangepicker/moment.min.js"></script>
	<script src="<?php echo base_url();?>assets/vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url();?>assets/vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url();?>assets/js/main.js"></script>
<!--===============================================================================================-->
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<!--===============================================================================================-->

<script>
document.getElementById("boasvindas").innerHTML = "Olá, " + name;
count = 2;
$("#mais").click(function(){
	if(count==2)
	{
		$('#tudo2').show();
	}
	if(count==3)
	{
		$('#tudo3').show();
	}
	if(count <= 3)
	{
	count++;
	}
	//console.log(count);
});

$("#remover2").click(function(){
	$('#name2').val('none');
	$('#qtd2').val('');
	$('#tudo2').hide();
	count --;
	//console.log(count);
});

$("#remover3").click(function(){
	$('#name3').val('none');
	$('#qtd3').val('');
	$('#tudo3').hide();
	count --;
	//console.log(count);
});

$('#tudo2').hide();
$('#tudo3').hide();
//-------------------------------------------------------------------------------------->
$("#pesquisar").click(function(){
	//alert($('#name1').val()+ ' ----- ' +$('#name2').val()+ ' ----- ' +$('#name3').val());
	//alert($('#qtd1').val()+ ' ----- ' +$('#qtd2').val()+ ' ----- ' +$('#qtd3').val());
	//console.log(count);
	var name1 = $('#name1').val();
	var name2 = $('#name2').val();
	var name3 = $('#name3').val();
	var qtd1 = $('#qtd1').val();
	var qtd2 = $('#qtd2').val();
	var qtd3 = $('#qtd3').val();
	var preco = $('#preco').val();
	var prazo = $('#prazo').val();
	var recomend = $('#recomend').val();

	if(preco == 'um' && prazo == 'dois' && recomend == 'tres' ||
	   preco == 'um' && prazo == 'tres' && recomend == 'dois' ||
	   preco == 'dois' && prazo == 'um' && recomend == 'tres' ||
	   preco == 'dois' && prazo == 'tres' && recomend == 'um' ||
	   preco == 'tres' && prazo == 'dois' && recomend == 'um' ||
	   preco == 'tres' && prazo == 'um' && recomend == 'dois'){
	   	if(name2=='none' && name3 == 'none'){//Só name 1 está preenchido
			$.post("<?php echo base_url();?>cLogadoLojista/oferta",
				{
				posicao:1,
				name1: name1,
				name2: name2,
				name3: name3,
				qtd1: qtd1,
				qtd2: qtd2,
				qtd3: qtd3,
				preco: preco,
				prazo: prazo,
				recomend: recomend
				},
				function(data){
				//swal(data);
				data = $.parseJSON(data);
			    $('#Modal').modal('show');

				$('#pro').val(data[0].nome);
				$('#qua').val(qtd1);
				$('#val').val('R$ '+ data[0].preco + ',00');
				var freteFinal = parseFloat(data[0].frete) * parseFloat(qtd1) * 0.10;
				$('#fre').val('R$ '+ data[0].frete + ',00');
				$('#est').val(data[0].recomend);
				$('#pra').val(data[0].prazo + ' dia');
				$('#for').val(data[0].fornecedor);
				var total = (parseFloat(data[0].preco) * parseFloat(qtd1) + parseFloat(data[0].frete));
				$('#tot').val('R$ '+ total + ',00');
		/*
			$('#qtd3').val(data[0].Comments);
			$('#preco').val(data[0].opp);
			$('#prazo').val(data[0].AE);
			$('#recomend').val(data[0].Comments);
		*/
			});
		}
		else if(name3=='none'){//ou seja, name 1 e name 2 estao preenchidos
			$.post("<?php echo base_url();?>cLogadoLojista/oferta",
				{
				posicao:2,
				name1: name1,
				name2: name2,
				name3: name3,
				qtd1: qtd1,
				qtd2: qtd2,
				qtd3: qtd3,
				preco: preco,
				prazo: prazo,
				recomend: recomend
				},
				function(data){
				//swal(data);
				data = $.parseJSON(data);
			    $('#Modal2').modal('show');
			    //salert(data[0].nome);
				$('#pro21').val(data[0].nome);
				$('#qua21').val(qtd1);
				$('#val21').val('R$ '+ data[0].preco + ',00');
				var freteFinal = parseFloat(data[0].frete) * parseFloat(qtd1) * 0.10;
				$('#fre21').val('R$ '+ data[0].frete + ',00');
				$('#est21').val(data[0].recomend);
				$('#pra21').val(data[0].prazo + ' dia');
				$('#for21').val(data[0].fornecedor);
				var total = (parseFloat(data[0].preco) * parseFloat(qtd1) + parseFloat(data[0].frete));
				$('#tot21').val('R$ '+ total + ',00');
				total21=total;
		/*
			$('#qtd3').val(data[0].Comments);
			$('#preco').val(data[0].opp);
			$('#prazo').val(data[0].AE);
			$('#recomend').val(data[0].Comments);
		*/
				$.post("<?php echo base_url();?>cLogadoLojista/oferta2",
				{
				posicao:2,
				name1: name1,
				name2: name2,
				name3: name3,
				qtd1: qtd1,
				qtd2: qtd2,
				qtd3: qtd3,
				preco: preco,
				prazo: prazo,
				recomend: recomend
				},
				function(data){
				//swal(data);
				data = $.parseJSON(data);
			    //salert(data[0].nome);
				$('#pro22').val(data[0].nome);
				$('#qua22').val(qtd2);
				$('#val22').val('R$ '+ data[0].preco + ',00');
				var freteFinal = parseFloat(data[0].frete) * parseFloat(qtd2) * 0.10;
				$('#fre22').val('R$ '+ data[0].frete + ',00');
				$('#est22').val(data[0].recomend);
				$('#pra22').val(data[0].prazo + ' dia');
				$('#for22').val(data[0].fornecedor);
				var total = (parseFloat(data[0].preco) * parseFloat(qtd2) + parseFloat(data[0].frete));
				$('#tot22').val('R$ '+ total + ',00');
				$('#totgeral').val('R$ '+ (total21 + total) + ',00');
				});
			});
		}
		else//ou seja, name 1 e name 2 e name 3 estao preenchidos
		{
		$.post("<?php echo base_url();?>cLogadoLojista/oferta",
				{
				posicao:2,
				name1: name1,
				name2: name2,
				name3: name3,
				qtd1: qtd1,
				qtd2: qtd2,
				qtd3: qtd3,
				preco: preco,
				prazo: prazo,
				recomend: recomend
				},
				function(data){
				//swal(data);
				data = $.parseJSON(data);
			    $('#Modal3').modal('show');
			    //salert(data[0].nome);
				$('#pro31').val(data[0].nome);
				$('#qua31').val(qtd3);
				$('#val31').val('R$ '+ data[0].preco + ',00');
				var freteFinal = parseFloat(data[0].frete) * parseFloat(qtd3) * 0.10;
				$('#fre31').val('R$ '+ data[0].frete + ',00');
				$('#est31').val(data[0].recomend);
				$('#pra31').val(data[0].prazo + ' dia');
				$('#for31').val(data[0].fornecedor);
				var total = (parseFloat(data[0].preco) * parseFloat(qtd3) + parseFloat(data[0].frete));
				$('#tot31').val('R$ '+ total + ',00');
				total31=total;
		/*		
			$('#qtd3').val(data[0].Comments);
			$('#preco').val(data[0].opp);
			$('#prazo').val(data[0].AE);
			$('#recomend').val(data[0].Comments);
		*/
				$.post("<?php echo base_url();?>cLogadoLojista/oferta2",
				{
				posicao:2,
				name1: name1,
				name2: name2,
				name3: name3,
				qtd1: qtd1,
				qtd2: qtd2,
				qtd3: qtd3,
				preco: preco,
				prazo: prazo,
				recomend: recomend
				},
				function(data){
				//swal(data);
				data = $.parseJSON(data);
			    //salert(data[0].nome);
				$('#pro32').val(data[0].nome);
				$('#qua32').val(qtd3);
				$('#val32').val('R$ '+ data[0].preco + ',00');
				var freteFinal = parseFloat(data[0].frete) * parseFloat(qtd3) * 0.10;
				$('#fre32').val('R$ '+ data[0].frete + ',00');
				$('#est32').val(data[0].recomend);
				$('#pra32').val(data[0].prazo + ' dia');
				$('#for32').val(data[0].fornecedor);
				var total = (parseFloat(data[0].preco) * parseFloat(qtd3) + parseFloat(data[0].frete));
				$('#tot32').val('R$ '+ total + ',00');
				total32=total;
					$.post("<?php echo base_url();?>cLogadoLojista/oferta3",
					{
					posicao:3,
					name1: name1,
					name2: name2,
					name3: name3,
					qtd1: qtd1,
					qtd2: qtd2,
					qtd3: qtd3,
					preco: preco,
					prazo: prazo,
					recomend: recomend
					},
					function(data){
					//swal(data);
					data = $.parseJSON(data);
				    //salert(data[0].nome);
					$('#pro33').val(data[0].nome);
					$('#qua33').val(qtd3);
					$('#val33').val('R$ '+ data[0].preco + ',00');
					var freteFinal = parseFloat(data[0].frete) * parseFloat(qtd3) * 0.10;
					$('#fre33').val('R$ '+ data[0].frete + ',00');
					$('#est33').val(data[0].recomend);
					$('#pra33').val(data[0].prazo + ' dia');
					$('#for33').val(data[0].fornecedor);
					var total = (parseFloat(data[0].preco) * parseFloat(qtd3) + parseFloat(data[0].frete));
					$('#tot33').val('R$ '+ total + ',00');
					$('#totgeral2').val('R$ '+ (total + total31 + total32) + ',00');
					});
				});
			});
		}
	}
	else
		{
			swal("O campo preço, prazo e recomendações devem ter peso distintos");
		}
});
</script>
</body>
</html>