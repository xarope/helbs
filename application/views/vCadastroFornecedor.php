<!DOCTYPE html>
<html lang="en">
<head>
	<title>Cadastro - Fornecedor</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="<?php echo base_url();?>assets/images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/fonts/iconic/css/material-design-iconic-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/vendor/select2/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/util.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/main.css">
<!--===============================================================================================-->
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<!--===============================================================================================-->
</head>
<body>
	
	<div class="limiter">
		<div class="container-login100" style="background-image: url('<?php echo base_url();?>assets/images/bg-01.jpg');">
			<div class="wrap-login100 p-l-55 p-r-55 p-t-65 p-b-54">
				<div class="login100-form validate-form">
					<span class="login100-form-title p-b-49">
						Cadastrar - Fornecedor
					</span>

					<div class="wrap-input100 validate-input">
						<span class="label-input100">Nome Completo</span>
						<input class="input100" type="text" id="nome" name = "nome" placeholder="Digite seu nome completo">
						<span class="focus-input100" data-symbol="&#x2616;"></span>
					</div>
					<br>
					<div class="wrap-input100 validate-input m-b-23" data-validate = "Usuário é necessário">
						<span class="label-input100">Usuário</span>
						<input class="input100" type="text" id="username" name="username" placeholder="Digite seu usuário">
						<span class="focus-input100" data-symbol="&#xf206;"></span>
					</div>

					<div class="wrap-input100 validate-input" data-validate="Senha é necessária">
						<span class="label-input100">Senha</span>
						<input i class="input100" type="password" id="pass" name="pass" placeholder="Digite sua senha">
						<span class="focus-input100" data-symbol="&#xF191;"></span>
					</div>
					<br>
					<div class="wrap-input100 validate-input" data-validate = "E-mail é necessário">
						<span class="label-input100">E-mail</span>
						<input class="input100" type="text" id="email" name="email" placeholder="Digite seu e-mail">
						<span class="focus-input100" data-symbol="&#9993;"></span>
					</div>
					<br>
					<div class="wrap-input100 validate-input" data-validate = "CNPJ é necessário">
						<span class="label-input100">CNPJ</span>
						<input class="input100" type="text" id="cnpj" name = "cnpj" placeholder="Digite seu CNPJ">
						<span class="focus-input100" data-symbol="&#x2616;"></span>
					</div>
					<br>
					<div class="wrap-input100 validate-input">
						<span class="label-input100">Endereço</span>
						<input class="input100" type="text" id="endereco" name = "endereco" placeholder="Digite seu endereço">
						<span class="focus-input100" data-symbol="&#x2616;"></span>
					</div>
					<br>
					<div class="wrap-input100 validate-input">
						<span class="label-input100">CEP</span>
						<input class="input100" type="text" id="cep" name = "cep" placeholder="Digite seu CEP">
						<span class="focus-input100" data-symbol="&#x2616;"></span>
					</div>
					<br>
					<div class="wrap-input100 validate-input">
						<span class="label-input100">CPF</span>
						<input class="input100" type="text" id="cpf" name = "cpf" placeholder="Digite seu CPF">
						<span class="focus-input100" data-symbol="&#x2616;"></span>
					</div>
					<br>


					<div class="container-login100-form-btn">
						<div class="wrap-login100-form-btn">
							<div class="login100-form-bgbtn"></div>
							<button id="CadastrarBtn" class="login100-form-btn">
								Cadastrar
							</button>
						</div>
					</div>

					<div class="txt1 text-center p-t-54 p-b-20">
						<span>
							Or Sign Up Using
						</span>
					</div>

					<div class="flex-c-m">
						<a href="#" class="login100-social-item bg1">
							<i class="fa fa-facebook"></i>
						</a>

						<a href="#" class="login100-social-item bg2">
							<i class="fa fa-twitter"></i>
						</a>

						<a href="#" class="login100-social-item bg3">
							<i class="fa fa-google"></i>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	

	<div id="dropDownSelect1"></div>
	
<!--===============================================================================================-->
	<script src="<?php echo base_url();?>assets/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url();?>assets/vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url();?>assets/vendor/bootstrap/js/popper.js"></script>
	<script src="<?php echo base_url();?>assets/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url();?>assets/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url();?>assets/vendor/daterangepicker/moment.min.js"></script>
	<script src="<?php echo base_url();?>assets/vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url();?>assets/vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url();?>assets/js/main.js"></script>

	<script>

	
$('#CadastrarBtn').click(function(){

	var cnpj = $('#cnpj').val();
	var email = $('#email').val();
	var pass = $('#pass').val();
	var username = $('#username').val();
	var nome = $('#nome').val();
	var cpf = $('#cpf').val();
	var cep = $('#cep').val();
	var endereco = $('#endereco').val();

$.post('<?php echo base_url();?>cCadastroFornecedor/contarlinhasLogin',
	function(data){
		data = $.parseJSON(data);
					$.post('<?php echo base_url();?>cCadastroFornecedor/insert_lojista',
								{	num:data[0].num,
						 			cnpj:cnpj,
						 			email:email,
						 			pass:pass,
						 			username:username,
						 			nome:nome,
						 			cpf:cpf,
						 			cep:cep,
						 			endereco:endereco
								},
								function(data){
									data = $.parseJSON(data);
									if(data==1){
											swal("Cadastro efetuado com sucesso", "Você será direcionado para a página de login", "success", {
											})
											.then((value) => {
											  window.location.href = "<?php echo base_url();?>cLoginFornecedor";
											});
											
									}
									else{
										swal("Opa !", "Parece que ocorreu um erro, tente novamente.", "error");
									}
								});//

						});
	});



</script>
</body>
</html>