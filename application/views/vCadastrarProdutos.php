<!DOCTYPE html>
<html lang="en">
<head>
	<title>Cadastrar - Produtos</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="<?php echo base_url();?>assets/images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/fonts/iconic/css/material-design-iconic-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/vendor/select2/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/util.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/main.css">
<!--===============================================================================================-->
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<!--===============================================================================================-->
<style>
</style>
</head>
<body>
	
	<div class="limiter">
		<div class="container-login100" style="background-image: url('<?php echo base_url();?>assets/images/bg-01.jpg');">
			<div class="wrap-login100 p-l-55 p-r-55 p-t-65 p-b-54">
				<div class="login100-form validate-form">
					<span class="login100-form-title p-b-49">
						Cadastrar Produtos
					</span>


					<div class="wrap-input100 validate-input m-b-23" >
						<span class="label-input100">Nome do produto</span>
						<input class="input100" type="text" id="nome_produto" name="nome_produto" placeholder="Digite aqui o nome do produto">
						<span class="focus-input100" data-symbol="&#x268C;"></span>
					</div>

					<div class="wrap-input100 validate-input" >
						<span class="label-input100">Preço (por Kilo)</span>
						<input i class="input100" type="text" id="preco" name="preco" placeholder="Digite aqui o preço">
						<span class="focus-input100" data-symbol="&#x27A3;"></span>
					</div>
					<br>
					<div class="wrap-input100 validate-input" >
						<span class="label-input100">Peso (Kg)</span>
						<input class="input100" type="text" id="peso" name="peso" placeholder="Digite aqui seu peso">
						<span class="focus-input100" data-symbol="&#x2696;"></span>
					</div>
					<br>

						<center><button type="button" class="btn btn-success" id="cadastrar">Cadastrar novo produto</button></center>

				</div>
			</div>
		</div>
	</div>
	

	<div id="dropDownSelect1"></div>
	
<!--===============================================================================================-->
	<script src="<?php echo base_url();?>assets/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url();?>assets/vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url();?>assets/vendor/bootstrap/js/popper.js"></script>
	<script src="<?php echo base_url();?>assets/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url();?>assets/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url();?>assets/vendor/daterangepicker/moment.min.js"></script>
	<script src="<?php echo base_url();?>assets/vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url();?>assets/vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url();?>assets/js/main.js"></script>

	<script>

$('#cadastrar').click(function(){	

	var nome_produto = $('#nome_produto').val();
	var preco = $('#preco').val();
	var peso = $('#peso').val();
	//alert(name);
	$.post('<?php echo base_url();?>cCadastrarProdutos/contarlinhasLogin',
	function(data){
		data = $.parseJSON(data);
		//alert(data[0].num);
		$.post('<?php echo base_url();?>cCadastrarProdutos/inserirProduto',
					{	num:data[0].num,
						nome_produto:nome_produto,
						preco:preco,
						peso:peso,
						name:name
					},
					function(data){
						data = $.parseJSON(data);
						if(data==1){
								swal("Cadastro efetuado com sucesso", "Você será direcionado para a página de login", "success", {
								})
								.then((value) => {
								  window.location.href = "<?php echo base_url();?>cLogadoFornecedor?name="+name;
								});
								
						}
						else{
							swal("Opa !", "Parece que ocorreu um erro, tente novamente.", "error");
						}
					});
	});
});

</script>
</body>
</html>