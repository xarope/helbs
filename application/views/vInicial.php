<!DOCTYPE html>
<html lang="en">
<head>
	<title>Home</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="<?php echo base_url();?>assets/images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/fonts/iconic/css/material-design-iconic-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/vendor/select2/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/util.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/main.css">
<!--===============================================================================================-->

<style type="text/css">
a:link, a:visited {
    
    color: white;
    padding: 14px 25px;
    text-align: center;
    text-decoration: none;
    margin-bottom: 20px;
        display: block;
  }
  #Logar{
  	background-color: #480ff9;
  }
  #Cadastrar{
  	background-color: #f44336;
  }
 h1{
 	background-color: #000;
 	color:#FFF;
 	padding:10px;
 }
 h2{
 	color:#333;
 	padding:10px;
 }
 h5{
 	background-color: #000;
 	color:#FFF;
 	padding:10px;
 	 font-family: Helvetica, Arial, sans-serif;
 }
#Logar, #Cfornecedor, #Ccomerciante, #Cadastrar, #Lfornecedor, #Lcomerciante{
 	display: block;
 	width: 100%;
 	margin-bottom: 10px;
 }
</style>


</head>
<body>
	
	<div class="limiter">
		<div class="container-login100" style="background-image: url('<?php echo base_url();?>assets/images/bg-01.jpg');">
			<div class="wrap-login100 p-l-55 p-r-55 p-t-65 p-b-54">
				<div class="login100-form validate-form">
					<span class="login100-form-title p-b-49">
						Helbs
					</span>
					  	<center><h1>Já pensou em receber a melhor oferta para sua loja com 1 clique?</h1>
					  	<br>
						<h2>Com o Helbs, você pode</h2></center>
						<br>
						<center><h5>Se conetar aos maiores fornecedores do Brasil<br>
						Obter os melhores pacotes de compras<br>
						Economizar seu tempo<br>
						E muito mais!</h5><center>
						<br>
						<br>
						<button type="button" class="btn btn-primary" id='Logar' >Logar</button>
						<button type="button" class="btn btn-danger" id='Cadastrar' >Cadastre-se</button>

				</div>
			</div>
		</div>
	</div>
	

	<div id="dropDownSelect1"></div>
<!--===============================================================================================-->
	<!-- Modal -->
<div id="Modal" class="modal fade" role="dialog">
  <div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
  <div class="modal-header">
    <h4 class="modal-title">Cadastre-se</h4>
     <button type="button" class="close" data-dismiss="modal">&times;</button>
  </div>
  <div class="modal-body">
	<button type="button" class="btn btn-light" id="Cfornecedor" >Sou Fornecedor</button>
	<button type="button" class="btn btn-dark" id="Ccomerciante">Sou Comerciante</button>
  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-danger" data-dismiss="modal">Fechar aba</button> 
  </div>
</div>

  </div>
</div>
<!--===============================================================================================-->
<!--===============================================================================================-->
	<!-- Modal -->
<div id="Modal2" class="modal fade" role="dialog">
  <div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
  <div class="modal-header">
    <h4 class="modal-title">Logue-se</h4>
     <button type="button" class="close" data-dismiss="modal">&times;</button>
  </div>
  <div class="modal-body">
	<button type="button" class="btn btn-light" id="Lfornecedor" >Sou Fornecedor</button>
	<button type="button" class="btn btn-dark" id="Lcomerciante">Sou Comerciante</button>
  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-danger" data-dismiss="modal">Fechar aba</button> 
  </div>
</div>

  </div>
</div>
<!--===============================================================================================-->
	<script src="<?php echo base_url();?>assets/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url();?>assets/vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url();?>assets/vendor/bootstrap/js/popper.js"></script>
	<script src="<?php echo base_url();?>assets/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url();?>assets/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url();?>assets/vendor/daterangepicker/moment.min.js"></script>
	<script src="<?php echo base_url();?>assets/vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url();?>assets/vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url();?>assets/js/main.js"></script>

	<script>
$('#Cadastrar').click(function(){	
$('#Modal').modal('show');
});
$('#Logar').click(function(){	
$('#Modal2').modal('show');
});


$('#Lcomerciante').click(function(){	
window.location.href = "<?php echo base_url();?>cLoginLojista";
});

$('#Lfornecedor').click(function(){	
window.location.href = "<?php echo base_url();?>cLoginFornecedor";
});

$('#Ccomerciante').click(function(){	
window.location.href = "<?php echo base_url();?>cCadastroLojista";
});

$('#Cfornecedor').click(function(){	
window.location.href = "<?php echo base_url();?>cCadastroFornecedor";
});


</script>
</body>
</html>